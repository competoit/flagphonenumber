import XCTest
@testable import FlagPhoneNumber

final class FlagPhoneNumberTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(FlagPhoneNumber().text, "Hello, World!")
    }
}
